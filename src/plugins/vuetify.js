import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import colors from 'vuetify/lib/util/colors'
Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
          light: {
            primary: '#3949AB',
            primary2: '#283593',
            secondary: '#FAFAFA',
            accent: '#0288D1',
            error: '#E53935',
        },
        dark: {
            primary: colors.grey.darken4,
            primary2: colors.grey.darken3,
            secondary: colors.grey.darken3,
            accent: colors.lightBlue.darken2,
            error: colors.red.darken1,
          },
          
        },
        
      },

});
