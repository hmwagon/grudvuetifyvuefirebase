import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/functions'

var firebaseConfig = {
    apiKey: "AIzaSyASAdQTYCcPHZBLHkTXVH9-iJDXrZ80hUA",
    authDomain: "vuetifyfirebase-1df1a.firebaseapp.com",
    databaseURL: "https://vuetifyfirebase-1df1a.firebaseio.com",
    projectId: "vuetifyfirebase-1df1a",
    storageBucket: "vuetifyfirebase-1df1a.appspot.com",
    messagingSenderId: "443487728380",
    appId: "1:443487728380:web:bb5d28ee188301c2879d79",
    measurementId: "G-2HQXGR8Q3E",
}
firebase.initializeApp(firebaseConfig)
const auth = firebase.auth()
const db = firebase.firestore()
const storage = firebase.storage()
const functions = firebase.functions()

export {
    firebase,
    auth,
    db,
    storage,
    functions
}

