import Vue from 'vue'
import VueRouter from 'vue-router'
import {firebase} from '../firebase/config'

Vue.use(VueRouter)

  const routes = [
  // { path: '*', component: () => import('../views/pages/404'),
  //   meta: {
  //     requiresAuth: false
  //   } 
  // },
  { path: '/', name: 'Home', component: () => import('../views/pages/Home'),
    meta: { 
      requiresAuth: true
    }
  },
  { path: '/proveedores', name: 'Proveedores', component: () => import('../views/pages/Proveedores'),
    meta: {
      requiresAuth: true
    }
  },
  { path: '/usuarios', name: 'Usuarios', component: () => import('../views/pages/RegistroUsuarios'),
    meta: {
      requiresAuth: true
    }
  },
  { path: '/login', name: 'Login', component: () => import('../views/pages/Login'),
    meta: {
      requiresAuth: false
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const aurentication = firebase.auth().currentUser;
  
  if (requiresAuth && !await aurentication){
    next('/login');
  }else{
    next();
  }


  // if(aurentication){
  //   console.log('La caca esta en el IF')
  //   next();
  // }else{
  //   if(requiresAuth && await aurentication != null){
  //     next();
  //     console.log('La caca esta en el IF del ELSE')
  //   }else{
  //     console.log('La caca esta en el ELSE del ELSE')
  //     next();
  //   }
  // }

  // if (requiresAuth && !await aurentication){
  // }else{
  //   next();
  // }


});

export default router
